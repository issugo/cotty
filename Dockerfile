FROM node:14

ADD . /app
WORKDIR /app
RUN npm install

RUN cd ~
RUN rm -rf /var/lib/cache/*
RUN rm -rf /var/lib/log/*

EXPOSE 3000

ENTRYPOINT ["node"]
CMD ["app.js", "-p", "3000"]
