# COTTY

## Prérequis  
node 12  
docker utilisable par l'utilisateur qui à lancer app.js (sudo marche bien mdr)

## Usage  
avant tout : il faut changer ```<adresse du script node>``` par l'ip de la vm et son port d'accès
### dans docker  
```bash
git clone https://gitlab.com/issugo/cotty.git   
docker build -t IMAGE_NAME .   
docker run -d -p 3000:3000 --name cotty IMAGE_NAME   
```

### sans docker
```node app.js -p <port>```
