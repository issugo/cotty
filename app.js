// requiring dependencies
const express = require('express')
const cors = require('cors')
const http = require('http')
const https = require('https')
const path = require('path')
const server = require('socket.io')
const pty = require('node-pty')
const fs = require('fs')

// predifined variables
const app = express()
const runhttps = false

var opts = require('optimist')
    .options({
        sslkey: {
            demand: false,
            description: 'path to SSL key'
        },
        sslcert: {
            demand: false,
            description: 'path to SSL certificate'
        },
        port: {
            demand: true,
            alias: 'p',
            description: 'cotty listen port'
        },
    }).boolean('allow_discovery').argv

// enabling cors request
app.use(cors())

process.on('uncaughtException', function(e) {
    console.error('Error: ' + e)
})

// defining solo endpoint
app.use('/', express.static(path.join(__dirname, 'public')))

// start the app
if (runhttps) {
    httpserv = https.createServer(opts.ssl, app).listen(opts.port, function() {
        console.log('https on port ' + opts.port)
    })
} else {
    httpserv = http.createServer(app).listen(opts.port, function() {
        console.log('http on port ' + opts.port)
    })
}

var io = server(httpserv,{
	path: '/',
	cors: {
		origins: "*",
		methods: ["GET", "POST"]
	}
})
io.on('connection', function(socket){
    var request = socket.request
    console.log((new Date()) + ' Connection accepted.')

    var term
    /*if (process.getuid() == 0) {
        term = pty.spawn('/bin/login', [], {
            name: 'xterm-256color',
            cols: 80,
            rows: 30
        });
    } else {
        term = pty.spawn('docker', ['run', '--rm', '-it', 'ubuntu:latest'], {
            name: 'xterm-256color',
            cols: 80,
            rows: 30
        });
    }*/
    term = pty.spawn('/bin/bash', [], {
            name: 'xterm-256color',
            cols: 80,
            rows: 30
        });
    term.on('data', function(data) {
	console.log("go output", data);
        socket.emit('output', data);
    });
    term.on('exit', function(code) {
        console.log((new Date()) + " PID=" + term.pid + " ENDED")
    });
    socket.on('resize', function(data) {
        term.resize(data.col, data.row);
    });
    socket.on('input', function(data) {
	console.log("go input", data);
        term.write(data);
    });
    socket.on('disconnect', function() {
        term.end();
    });
})

