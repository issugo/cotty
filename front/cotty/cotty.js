function $_GET(param) {
	var vars = {};
	window.location.href.replace( location.hash, '' ).replace( 
		/[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
		function( m, key, value ) { // callback
			vars[key] = value !== undefined ? value : '';
		}
	);

	if ( param ) {
		return vars[param] ? vars[param] : null;	
	}
	return vars;
}

let levelName = $_GET("level");

var socket = io('<adresse du script node>', {
    transports: ['websocket'],
    'force new connection': true
})
var buf = '';

function Cotty(argv) {
    this.argv_ = argv;
    this.io = null;
    this.pid_ = -1;
}

Cotty.prototype.run = function () {
    this.io = this.argv_.io.push();

    this.io.onVTKeystroke = this.sendString_.bind(this);
    this.io.sendString = this.sendString_.bind(this);
    this.io.onTerminalResize = this.onTerminalResize.bind(this);
}

Cotty.prototype.sendString_ = function (str) {
    socket.emit('input', str);
};

Cotty.prototype.onTerminalResize = function (col, row) {
    socket.emit('resize', {
        col: col,
        row: row
    });
};

socket.on('connect', function () {
    console.log("connected");
    socket.emit("createLevel", {level: levelName});
    console.log(socket.id);
    lib.init(function () {
        hterm.defaultStorage = new lib.Storage.Local();
        term = new hterm.Terminal();
        window.term = term;
        term.decorate(document.getElementById('terminal'));

        term.setCursorPosition(0, 0);
        term.setCursorVisible(true);
        term.prefs_.set('ctrl-c-copy', true);
        term.prefs_.set('ctrl-v-paste', true);
        term.prefs_.set('use-default-window-copy', true);

        term.runCommandClass(Cotty, document.location.hash.substr(1));
        socket.emit('resize', {
            col: term.screenSize.width,
            row: term.screenSize.height
        });

        if (buf && buf != '') {
            term.io.writeUTF16(buf);
            buf = '';
        }

        window.parent.postMessage('terminal_loaded', '*');
    });
});

socket.on('output', function (data) {
    console.log(data);
    if (!term) {
        buf += data;
        return;
    }
    term.io.writeUTF16(data);
});

socket.on('disconnect', function () {
    console.log("Socket.io connection closed");
});

socket.emit("bonjour", "world");